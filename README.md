# eatt &mdash; unofficial implementation of EA-TT in Haskell
An *unofficial, incomplete* implementation of the Elementary Affine Type Theory in
Haskell, with some[^1] changes. See [here][1] for more information about
EA-TT.

[^1]: See [here](NONCOMPLIANCE.md)

### Usage
````
eatt, version <version>, compiled <time>

Usage: eatt [options] arg...

Options:
  -v                Print version and exit
  -h                Print this help information and exit
  -i                Print extra information

  -T                Perform type checking     [NYI]
  -N                Print type nominalised    [NYI]
  -m                Print term unnominalised
  -n                Print term nominalised    [NYI]
  -e                Perform type erasure      [NYI]
  -d                List all declarations

Arguments that contain a '/' are understood to refer to files. To match
all files in the current working directory, use './*.eatt' (this should)
be expanded by your shell).
````

### Example EA-TT programs
* Official [main.eatt](extra/example.eatt)

### External links
* [Documentation for EA-TT][1]
* [Spec for EA-TT][2]

### Disclaimer
This is an __unofficial__ implementation of EA-TT. *insert disclaimer here*

[1]: https://gitlab.com/moonad/formality/blob/master/docs/EA-TT.md
[2]: https://gitlab.com/moonad/formality/blob/master/spec/EA-TT.md
[3]: src/Parser1.y
