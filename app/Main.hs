{-# LANGUAGE LambdaCase #-}

module Main where

import Control.Monad
import Data.Either
import Data.List
import System.Environment
import System.Exit

import qualified Common
import qualified Parser
import qualified PP

intendedUsage :: String -> String
intendedUsage progName = unlines
  [progName ++ ", version " ++ VERSION_eatt ++ ", compiled " ++ __TIME__
  ,""
  ,"Usage: " ++ progName ++ " [options] arg..."
  ,""
  ,"Options:"
  ,"  -v                Print version and exit"
  ,"  -h                Print this help information and exit"
  ,"  -i                Print extra information"
  ,""
  ,"  -T                Perform type checking     [NYI]"
  ,"  -N                Print type nominalised    [NYI]"
  ,"  -m                Print term unnominalised"
  ,"  -n                Print term nominalised    [NYI]"
  ,"  -e                Perform type erasure      [NYI]"
  ,"  -d                List all declarations"
  ,""
  ,"Arguments that contain a '/' are understood to refer to files. To match"
  ,"all files in the current working directory, use './*.eatt' (this should"
  ,"be expanded by your shell)."
  ]

{-
  Intended usage: see above.

  The usage of this program is intended to mirror how the standard
  implementation of EA-TT is invoked.

  DEFAULT IMPLEMENTATION'S RESPONSE

    Elementary Affine Type Theory (EA-TT)
    
    Usage: eatt [options] expr
    (loads local .eatt files and runs/checks an expr)
    
    Options:
    -v shows EA-TT version
    -i shows extra information
    -N shows type normalized
    -R shows type with unexpanded references
    -W shows type on weak normal form
    -L shows type on LAM form instead of EA-CORE
    -E shows type erased
    -n shows term
    -r shows term with unexpanded references
    -w shows term on weak normal form
    -l shows term on LAM form instead of EA-CORE
    -e shows term erased
    -x shows NASIC evaluation
    (default: -inleNRx)
-}

main :: IO ()
main = do
  args'' <- getArgs
  let Args flags args' = parseArgs args''

  args <- resolveArgs args'

  let (files, terms) = partitionEithers args

  let printInfo = 'i' `elem` flags

  let useArgs = not . null $ "TNmned" `intersect` flags

  when ('v' `elem` flags) $ do
    progName <- getProgName
    putStrLn $ progName ++ " version " ++ VERSION_eatt ++ ", compiled " ++ __TIME__
    exitSuccess
  when ('h' `elem` flags || null args'') $ do
    progName <- getProgName
    putStr $ intendedUsage progName
    exitSuccess
  
  when useArgs $ do
    parsedFiles <- forM files $ \case
      (name, content) -> do
        when printInfo $ putStrLn $ "File " ++ name
        
        either die pure $ Parser.parseDecls content (Parser.TextPosn 0 0)

    when ('d' `elem` flags) $ do
      when printInfo $ putStrLn "Declarations:"
      forM_ parsedFiles $ \fileDecls ->
        forM_ fileDecls $ \(Common.Decl name _) -> putStrLn $ "* " ++ name

    forM_ terms $ \term -> do
      when printInfo $ putStrLn $ "Term " ++ cropTermText term
      
      case Parser.parseTerm term (Parser.TextPosn 0 0) of
        Left  err        -> die err
        Right parsedTerm ->
          when ('m' `elem` flags) $ putStrLn $ PP.ppTerm parsedTerm

cropTermText :: String -> String
cropTermText a | length a > 15 = show $ take 15 a ++ "..."
               | otherwise     = show a

debug :: Show a => a -> IO ()
debug a = putStrLn $ "[DEBUG]: " ++ show a

data Args = Args [Char] [String]

instance Semigroup Args where
  Args a b <> Args a' b' = Args (a ++ a') (b ++ b')

parseArg :: String -> Args
parseArg ('-' : a) = Args a  []
parseArg a         = Args [] [a]

parseArgs :: [String] -> Args
parseArgs = foldr ((<>) . parseArg) (Args [] [])

--              Name    Content
type FileArg = (String, String)

resolveArg :: String -> IO (Either FileArg String)
resolveArg a | '/' `elem` a = do
  content <- readFile a
  pure $ Left (a, content)
             | otherwise = pure $ Right a

resolveArgs :: [String] -> IO [Either FileArg String]
resolveArgs = mapM resolveArg
