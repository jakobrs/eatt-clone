# Noncompliance between eatt and EA-TT

The grammar and lexer are located [here](src/Parser.y).

## Lexer

*   Variables can't start with a dot
    
    ```
    Without this change
    "  .Nat.0" ==> (DOT) (VAR "Nat.0")
    ". .Nat.0" ==> (DOT) (VAR ".Nat.0")

    With this change
    "  .Nat.0" ==>       (DOT) (Var "Nat.0")
    ". .Nat.0" ==> (DOT) (DOT) (Var "Nat.0")
    ```

## Parser

* Letrec-like construction might be added in the future\
  ```
  Term -> let Decl Decls Term
  ```
