{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

module Common where

data Token
  = TokenDot
  | TokenColon
  | TokenEqual
  | TokenOP
  | TokenCP
  | TokenNew
  | TokenUse
  | TokenSelf
  | TokenOS
  | TokenCS
  | TokenOB
  | TokenCB
  | TokenMinus
  | TokenPut
  | TokenBox
  | TokenLet
  | TokenVar String
  | TokenEOF
  deriving (Show, Eq, Ord)

type Name = String

-- | Pretty-printer for names
class NameI a where
  -- | Pretty-print a name
  ppName :: a -> String

instance NameI Name where
  ppName = id

instance NameI Int where
  ppName n = "#" ++ show n

instance NameI (Name, Int) where
  ppName (a, n) = a ++ "#" ++ show n

data TermG name
  = TVar  name
  | TSlf  Name         (TermG name)
  | TNew  (TermG name) (TermG name)
  | TUse  (TermG name)
  | TLam  Bool         (Maybe Name) (Maybe (TermG name)) (TermG name)
  | TDup  Name         (TermG name) (TermG name        )
  | TArr  Bool         (Maybe Name) (TermG name        ) (TermG name)
  | TApp  (TermG name) (TermG name)
  | TAppE (TermG name) (TermG name)
  | TPut  (TermG name)
  | TBox  (TermG name)
  | TLet  Name         (TermG name) (TermG name)
  | TTyp  (TermG name) (TermG name)
  deriving (Show, Eq, Ord)

-- | Non-deBruijn-indexed terms
type Term = TermG Name
-- | DeBruijn-indexed terms
type TermDB = TermG Int

-- | Erased term
data ETerm
  = EVar Name
  | ELam (Maybe Name) ETerm
  | EDup Name ETerm ETerm
  | EApp ETerm ETerm
  | EPut ETerm
  | ELet Name ETerm ETerm
  deriving (Show, Eq, Ord)

-- | Declaration
data Decl = Decl Name Term deriving (Show, Eq, Ord)

collectArgs :: TermG name -> (TermG name, [(Bool, TermG name)])
collectArgs arg =
    let (t, args) = go arg
    in  (t, reverse args)
  where
    go :: TermG name -> (TermG name, [(Bool, TermG name)])
    go (TApp a b) =
      let (t, args) = go a
      in  (t, (False, b) : args)
    go (TAppE a b) =
      let (t, args) = go a
      in  (t, (True, b) : args)
    go a = (a, [])
