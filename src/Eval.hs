module Eval where

import Data.List

import Common

bruijnise :: Term -> TermDB
bruijnise = go []
  where
    go :: [Maybe Name] -> Term -> TermDB
    go g (TVar a) = case elemIndex (Just a) g of
      Just i -> TVar i
      Nothing -> error $ "Unbound variable " ++ a
    go g (TSlf a b)     = TSlf  a        (go g b)
    go g (TNew a b)     = TNew  (go g a) (go g b)
    go g (TUse a)       = TUse  (go g a)
    go g (TPut a)       = TPut  (go g a)
    go g (TBox a)       = TBox  (go g a)
    go g (TTyp a b)     = TTyp  (go g a) (go g b)
    go g (TLet a b c)   = TLet  a        (go g b) (go (Just a : g) c)
    go g (TDup a b c)   = TDup  a        (go g b) (go (Just a : g) c)
    go g (TApp a b)     = TApp  (go g a) (go g b)
    go g (TAppE a b)    = TAppE (go g a) (go g b)
    go g (TLam e n t b) = TLam  e n (go g <$> t) (go (n : g) b)
    go g (TArr e n t b) = TArr  e n (go g     t) (go (n : g) b)

{-
succName :: Name -> Name
succName n =
  let (d, a) = span isDigit $ reverse n
  in  reverse a ++ show (succ (read (reverse digits)))

-- | Perform a substitution in a term
subst
  :: Name -- ^ The variable to substitute
  -> Term -- ^ The term to substitute the variable with
  -> Term -- ^ The term to substiture the variable with the term in
  -> Term -- ^ Resulting term
subst v r (TVar n)   | n == v    = r
                     | otherwise = TVar n
subst v r (TApp a b)             = App (subst n r a) (subst n r b)
subst v r (TLam n b) | n == v    = Lam n' (subst v r (subst n (Var n') b))
                     | otherwise = Lam n  (subst v r                   b )
  where
    n' = succName n
subst v r (TSlf n b) | n == v    = TSlf n' (subst v r (subst n (TVar n') b))
                     | otherwise = TSlf n  (subst v                      b )
  where
    n' = succName n
subst v r (TUse a)               = TUse $ subst v r
subst v r (TPut a)               = TPut $ subst v r
subst v r (TBox a)               = TBox $ subst v r
subst v r (TTyp a b)             = TTyp (subst v r a) (subst v r b)
subst _ _ _                      = error "NYI: subst"

eval :: Term -> Term
eval _ = error "NYI: eval"
-}

eraseE :: Term -> Maybe ETerm
eraseE (TVar a) = pure $ EVar a
eraseE (TSlf _ _) = Nothing
eraseE (TNew _ _) = Nothing
eraseE (TUse a) = eraseE a
eraseE (TLam True  _ _ c) = eraseE c
eraseE (TLam False a _ c) = ELam a <$> eraseE c
eraseE (TDup a b c) = EDup a <$> eraseE b <*> eraseE c
eraseE TArr{} = Nothing
eraseE (TApp a b) = EApp <$> eraseE a <*> eraseE b
eraseE (TAppE a _) = eraseE a
eraseE (TPut a) = EPut <$> eraseE a
eraseE (TBox _) = Nothing
eraseE (TLet a b c) = ELet a <$> eraseE b <*> eraseE c
eraseE (TTyp _ b) = eraseE b

erase :: Term -> Maybe Term
erase (TVar a) = pure $ TVar a
erase (TSlf _ _) = Nothing
erase (TNew _ _) = Nothing
erase (TUse a) = erase a
erase (TLam True _ _ c) = erase c
erase (TLam False a _ c) = TLam False a Nothing <$> erase c
erase (TDup a b c) = TDup a <$> erase b <*> erase c
erase TArr{} = Nothing
erase (TApp a b) = TApp <$> erase a <*> erase b
erase (TAppE a _) = erase a
erase (TPut a) = TPut <$> erase a
erase (TBox _) = Nothing
erase (TLet a b c) = TLet a <$> erase b <*> erase c
erase (TTyp _ b) = erase b
