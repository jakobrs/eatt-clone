-- Parser for Elementary Affine Type Theory

{
{-# LANGUAGE BangPatterns #-}

module Parser where

import Data.Char
import Data.List

import Common
import PP
}

%name parseDecls Decls
%name parseDecl  Decl
%name parseTerm  Term

%tokentype { Token }
%errorhandlertype explist
%error { parseError }

%lexer { lexer } { TokenEOF }
%monad { Parser } { thenP } { returnP }

%token
    '.'       { TokenDot    }
    ':'       { TokenColon  }
    '='       { TokenEqual  }
    '('       { TokenOP     }
    ')'       { TokenCP     }
    '@'       { TokenNew    }
    '~'       { TokenUse    }
    '$'       { TokenSelf   }
    '['       { TokenOS     }
    ']'       { TokenCS     }
    '{'       { TokenOB     }
    '}'       { TokenCB     }
    '-'       { TokenMinus  }
    '|'       { TokenPut    }
    '!'       { TokenBox    }
    let       { TokenLet    }
    var       { TokenVar $$ }

%%

Decls :: { [Decl] }
Decls: Decl Decls                           { $1 : $2 }
     | {- empty -}                          { [] }

Decl :: { Decl }
Decl: '.' var Term                          { Decl $2 $3 }

Term :: { Term }
Term: '$' var Term                          { TSlf $2 $3 }
    | '@' Term Term                         { TNew $2 $3 }
    | '~' Term                              { TUse $2 }
    | '['                  ']' Term         { TLam False Nothing   Nothing   $3 }
    | '[' '-'              ']' Term         { TLam True  Nothing   Nothing   $4 }
    | '['         ':' Term ']' Term         { TLam False Nothing   (Just $3) $5 }
    | '[' '-'     ':' Term ']' Term         { TLam True  Nothing   (Just $4) $6 }
    | '['     var          ']' Term         { TLam False (Just $2) Nothing   $4 }
    | '[' '-' var          ']' Term         { TLam True  (Just $3) Nothing   $5 }
    | '['     var ':' Term ']' Term         { TLam False (Just $2) (Just $4) $6 }
    | '[' '-' var ':' Term ']' Term         { TLam True  (Just $3) (Just $5) $7 }
    | '{'         ':' Term '}' Term         { TArr False Nothing   $3 $5 }
    | '{' '-'     ':' Term '}' Term         { TArr True  Nothing   $4 $6 }
    | '{'     var ':' Term '}' Term         { TArr False (Just $2) $4 $6 }
    | '{' '-' var ':' Term '}' Term         { TArr True  (Just $3) $5 $7 }
    | '[' var '=' Term ']' Term             { TDup $2 $4 $6 }
    | '(' Term Args ')'                     { tApp $2 $3 }
    | '|' Term                              { TPut $2 }
    | '!' Term                              { TBox $2 }
    | ':' Term '=' Term                     { TTyp $2 $4 }
    | let var Term Term                     { TLet $2 $3 $4 }
--  | let Decl Decls Term                   { TLetrec ($2 : $3) $4 }
    | var                                   { TVar $1 }

Args :: { [(Bool, Term)] }
Args: {- empty -}                           { [] }
    | Term Args                             { (False, $1) : $2 }
    | '-' Term Args                         { (True , $2) : $3 }

{
tApp :: TermG name -> [(Bool, TermG name)] -> TermG name
tApp a ((False, arg):b) = tApp (TApp  a arg) b
tApp a ((True , arg):b) = tApp (TAppE a arg) b
tApp a [] = a

varChars :: String
varChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._~"

--lexerer :: (Token -> Parser a) -> Parser a
--lexerer = (lexer returnP `thenP`)

rightN :: TextPosn -> TextPosn
rightN (TextPosn r c) = TextPosn r (succ c)

incN :: Int -> TextPosn -> TextPosn
incN d (TextPosn r c) = TextPosn r (c + d)

returnN :: TextPosn -> TextPosn
returnN (TextPosn r _) = TextPosn (succ r) 0

-----------------------------------------------------------------
-- LEXER LEXER LEXER LEXER LEXER LEXER LEXER LEXER LEXER LEXER --
-----------------------------------------------------------------
lexer :: (Token -> Parser a) -> Parser a
lexer _    _        !l | False = undefined
lexer cont (':' : a) l = cont TokenColon a (rightN l)
lexer cont ('$' : a) l = cont TokenSelf  a (rightN l)
lexer cont ('@' : a) l = cont TokenNew   a (rightN l)
lexer cont ('~' : a) l = cont TokenUse   a (rightN l)
lexer cont ('(' : a) l = cont TokenOP    a (rightN l)
lexer cont (')' : a) l = cont TokenCP    a (rightN l)
lexer cont ('[' : a) l = cont TokenOS    a (rightN l)
lexer cont (']' : a) l = cont TokenCS    a (rightN l)
lexer cont ('{' : a) l = cont TokenOB    a (rightN l)
lexer cont ('}' : a) l = cont TokenCB    a (rightN l)
lexer cont ('-' : a) l = cont TokenMinus a (rightN l)
lexer cont ('=' : a) l = cont TokenEqual a (rightN l)
lexer cont ('.' : a) l = cont TokenDot   a (rightN l)
lexer cont ('|' : a) l = cont TokenPut   a (rightN l)
lexer cont ('!' : a) l = cont TokenBox   a (rightN l)

lexer cont   ('\n' : a) l                     = lexer cont a (returnN l)
lexer cont   (c    : a) l | isSpace c         = lexSpace cont a l
lexer cont a@(c    : _) l | isVarChar c       = lexVar cont a l
lexer cont   []         l                     = cont TokenEOF (error "Happy seems to have tried to scan past the end "
                                                                  ++ "of the file. This is probably a bug.") (rightN l)
lexer cont   ('/' : '/' : a) l =
  let (r, l') = dropComment a l
  in  lexer cont r l'
lexer cont   (c    : _) l = Left $ "Lexical error on " ++ ppPosn l
                                ++ ": unexpected character " ++ show c

lexSpace :: (Token -> Parser a) -> Parser a
lexSpace cont a l =
  let (space, rest) = span isSpace a
  in  lexer cont rest (incN (length space + 1) l)

isVarChar :: Char -> Bool
isVarChar a | isAlphaNum a = True
            | a `elem` ".~_" = True
            | otherwise = False

lexVar :: (Token -> Parser a) -> Parser a
lexVar cont a l =
  let (var, rest) = span isVarChar a
  in  if var == "let"
        then cont  TokenLet      rest (incN 3 l)
        else cont (TokenVar var) rest (incN (length var) l)

dropComment :: String -> TextPosn -> (String, TextPosn)
dropComment ('\n' : a) l = (a, returnN l)
dropComment (_    : a) l = dropComment a (rightN l)
dropComment ""         l = ("", rightN l)
-----------------------------------------------------------------
-- END LEXER END LEXER END LEXER END LEXER END LEXER END LEXER --
-----------------------------------------------------------------

data TextPosn = TextPosn !Int !Int
type Parser a = String -> TextPosn -> Either String a

returnP :: a -> Parser a
returnP a s l = Right a

thenP :: Parser a -> (a -> Parser b) -> Parser b
thenP f k s l = case f s l of
  Left e -> Left e
  Right a -> k a s l

failP :: String -> Parser a
failP err s l = Left err

getPosn :: Parser TextPosn
getPosn s l = Right l

ppPosn :: TextPosn -> String
ppPosn (TextPosn r c) = "line " ++ show r ++ ", column " ++ show c

{-
split :: Int -> [a] -> [[a]]
split _ [] = []
split l a = take l a : split l (drop l a)
-}

ppExpList :: [String] -> String
ppExpList [a] = a
ppExpList a = intercalate ", " (init a) ++ ", or " ++ last a

parseError :: (Token, [String]) -> Parser a
parseError (a, p) = getPosn `thenP` \l ->
                      failP $ "Parse error on " ++ ppPosn l ++ ":\n"
                           ++ "Unexpected '" ++ ppToken a ++ "' (" ++ show a ++ ")"
                           ++ if null p then "" else "\n  Expected " ++ ppExpList p
--                    ++ "\n"
--                    ++ "Rest of file:"
--                    ++ concatMap ("\n  " ++) (split 70 (ppTokens a))
}

-- vim: syntax=haskell
