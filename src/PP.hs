{-# LANGUAGE ViewPatterns #-}

module PP where

import Data.List

import Common

ppToken :: Token -> String
ppToken TokenDot     = "."
ppToken TokenColon   = ":"
ppToken TokenEqual   = "="
ppToken TokenOP      = "("
ppToken TokenCP      = ")"
ppToken TokenNew     = "@"
ppToken TokenUse     = "~"
ppToken TokenSelf    = "$"
ppToken TokenOS      = "["
ppToken TokenCS      = "]"
ppToken TokenOB      = "{"
ppToken TokenCB      = "}"
ppToken TokenMinus   = "-"
ppToken TokenPut     = "|"
ppToken TokenBox     = "!"
ppToken TokenLet     = "let"
ppToken (TokenVar a) = a
ppToken TokenEOF     = "<EOF>"

ppTokens :: [Token] -> String
ppTokens [] = ""
ppTokens [a] = ppToken a
ppTokens (a:b) = ppToken a ++ " " ++ ppTokens b

ppTerm :: NameI name => TermG name -> String
ppTerm (TVar a) = ppName a
ppTerm (TSlf a b) = "$" ++ a ++ " " ++ ppTerm b
ppTerm (TNew a b) = "@" ++ ppTerm a ++ " " ++ ppTerm b
ppTerm (TUse a) = "~" ++ ppTerm a
ppTerm (TLam False Nothing  Nothing  b) = "[] " ++ ppTerm b
ppTerm (TLam True  Nothing  Nothing  b) = "[-] " ++ ppTerm b
ppTerm (TLam False (Just n) Nothing  b) = "[" ++ n ++ "] " ++ ppTerm b
ppTerm (TLam True  (Just n) Nothing  b) = "[-" ++ n ++ "] " ++ ppTerm b
ppTerm (TLam False Nothing  (Just t) b) = "[: " ++ ppTerm t ++ "] " ++ ppTerm b
ppTerm (TLam True  Nothing  (Just t) b) = "[-: " ++ ppTerm t ++ "] " ++ ppTerm b
ppTerm (TLam False (Just n) (Just t) b) = "[" ++ n ++ " : " ++ ppTerm t ++ "] " ++ ppTerm b
ppTerm (TLam True  (Just n) (Just t) b) = "[-" ++ n ++ " : " ++ ppTerm t ++ "] " ++ ppTerm b
ppTerm (TArr False Nothing  t        b) = "{: " ++ ppTerm t ++ "} " ++ ppTerm b
ppTerm (TArr True  Nothing  t        b) = "{-: " ++ ppTerm t ++ "} " ++ ppTerm b
ppTerm (TArr False (Just n) t        b) = "{" ++ n ++ " : " ++ ppTerm t ++ "} " ++ ppTerm b
ppTerm (TArr True  (Just n) t        b) = "{-" ++ n ++ " : " ++ ppTerm t ++ "} " ++ ppTerm b
ppTerm (TDup n t b) = "[" ++ n ++ " = " ++ ppTerm t ++ "] " ++ ppTerm b
ppTerm (TPut t) = "|" ++ ppTerm t
ppTerm (TBox t) = "! " ++ ppTerm t
ppTerm (TLet n t b) = "let " ++ n ++ " " ++ ppTerm t ++ " " ++ ppTerm b
ppTerm (TTyp t b) = ": " ++ ppTerm t ++ " = " ++ ppTerm b
ppTerm a@TApp{} =
  let (t, r) = collectArgs a
  in  "(" ++ ppTerm t ++ ppArgs r ++ ")"
ppTerm a@TAppE{} =
  let (t, r) = collectArgs a
  in  "(" ++ ppTerm t ++ ppArgs r ++ ")"

ppArg :: NameI name => (Bool, TermG name) -> String
ppArg (False, a) = " " ++ ppTerm a
ppArg (True , a) = " -" ++ ppTerm a

ppArgs :: NameI name => [(Bool, TermG name)] -> String
ppArgs = concatMap ppArg

ppDecl :: Decl -> String
ppDecl (Decl n b) = ". " ++ n ++ " " ++ ppTerm b

ppDecls :: [Decl] -> String
ppDecls a = concat $ intersperse "\n" $ map ppDecl a
